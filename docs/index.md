# Homadorma Api



## Search suggestion

* ```GET api/search-by```

+ Return a collection of schools when user searches for a certain keywords. Used in the result suggestion for the search box on homadorma.
+ **Resource URL**
	+ ``` https://api.zhuwojia.net/api/search-by?type=(institution tupe)&q=(key word) ```
+ **Resource Information**

	+	|  |  |
		| ------ | ----------- |
		| Response formats   | JSON |
		| Requires authentication?	 | Yes|
		| Rate limited    | No |

+ **Parameters**
	+	| Name | Required  | Description | Default Value | Example
		| ------ | ----------- | ---------- | ---------- | ------ |
		| type   | yes | <ul>One of these values : <li>"all"</li><li>"elem", stands for elementary schools</li><li>"high", stands for secondary schools</li><li>"college-university", stands for colleges and universities</li><li>"language-school", stands for language schools</li><li>"city"</li></ul> | | all
		| q	 | yes| The search keyword | | toronto

+ **Example Request**
	+ ``` https://api.zhuwojia.net/api/search-by?type=college-university&q=toronto ```
+ **Example Response**

	+ ``` 
    [{
        "id": 14639,
        "name": "Lambton College - Toronto",
        "name_zh": "莱姆顿学院多伦多校区",
        "board": null,
        "board_zh": null,
        "level": null,
        "type": "College",
        "address": "400-265 Yorkland Blvd",
        "city": "Toronto",
        "province": "Ontario",
        "zip": "M2J1S5",
        "lat": 43.773338,
        "uid": "",
        "lng": -79.33603,
        "suggestion_name": "LambtonCollegeToronto"
    }]  ```		

## Homestay search by School id or City id

* ```GET api/search/homestay```
+ Return a collection of homestay information near a school or a city. It functions similar as the search page on homadorma.
+ **Resource URL**
	+ ``` https://api.zhuwojia.net/api/search/homestay ```
+ **Resource Information**

	+	|  |  |
		| ------ | ----------- |
		| Response formats   | JSON |
		| Requires authentication?	 | Yes|
		| Rate limited    | No |

+ **Parameters**
	+	| Name | Required  | Description | Default Value | Example
		| ------ | ----------- | ---------- | ---------- | ------ |
		| type   | yes | One of these values : *school*, *city* | | school
		| id   | yes | ID of the school or city | |5926
		| search_type   | no | Returns the *short* or *full* version. Short version skips the room specific information to improve loading speed. Short version is displayed by default | |full
		| student_id	 | optional| the student id | | 22 
		| pet	 | optional| pet filter search . Values : *no_cat*, *no_dog*, *no_all* |  | no_cat 
		| floor_level	 | optional| floor level filter search . Values : *above_ground*, *basement* |  | above_ground 
		|move_in_date | optional| Select the move in available date | | 2020-02-21 
		|meal | optional | Select host provide meal or not. value : *with_meal*, *no_meal* | | with_meal
		|min_price | optional | Filter with the minimum price. The return includes homestays with fee, or fee+meal_price matching the criteria. The price filter only works in search_type=full  | | 800
		|max_price |optional | Filter with the maximum price. The return includes homestays with fee, or fee+meal_price matching the criteria. The price filter only works in search_type=full | | 2000

+ **Response Field Explained**
	+	| Name | Description | Example
		| ------ | ----------- | ------ |
		| fee | lowest value of above the ground rooms | 800
		| charge | lowest value of basement rooms | 600
		| total_rooms | the total number of student rooms in a homestay | 3

**Example Request**
	+ ``` https://api.zhuwojia.net/api/search/homestay?type=school&id=5926 ```

+ **Example Response**

	+ ``` 
    [{
        "id": "HS100016",
        "id_db": 16,
        "preferred_name": "Rosalina",
        "resitype": "dhouse",
        "total_room": 2,
        "images": [],
        "tmember": "1",
        "fee": 1000,
        "meal_price": 0,
        "distance": 5.8,
        "lat": 43.79737594585236,
        "lng": -79.45705298038536,
        "school_boards": [
            {
                "id": 371,
                "en": "York Catholic District School Board",
                "zh": "约克天主教教育局",
                "path": "assets/imgs/badges/YCDSB.svg"
            },
            {
                "id": 372,
                "en": "York Region District School Board",
                "zh": "约克区公立教育局",
                "path": "assets/imgs/badges/YRDSB.svg"
            }
        ],
        "badges": {
            "interview": true,
            "interview_student": true,
            "police_check": true,
            "institution": true,
            "area": [
                {
                    "id": 371,
                    "en": "York Catholic District School Board",
                    "zh": "约克天主教教育局",
                    "path": "assets/imgs/badges/YCDSB.svg"
                },
                {
                    "id": 372,
                    "en": "York Region District School Board",
                    "zh": "约克区公立教育局",
                    "path": "assets/imgs/badges/YRDSB.svg"
                }
            ],
            "background_check_year": "4"
        },
        "interview_staff": true,
        "interview_student": true,
        "background": true,
        "selfintro": "Hi my name is Rosalina. I've been hosting international students for 15 years. I love meeting new friends and learn their culture and food. \n",
        "score": 30,
        "meal": "yyy",
        "gpr": "f",
        "minage": "np",
        "maxage": "np",
        "agroom": 2,
        "bsroom": 0,
        "sharewash": "1",
        "agavail": "2020-08-21",
        "bravail": "2020-08-21",
        "smoke": "n",
        "inst": true
    }]  ```

## Commute time

* ```GET api/homestays/commute```
+ Return the commute time between a homestay and a school. By default, the call looks for transit time in our database. If no such information could be obtained, it will attempt to look up through Google Maps API using the key of zhuwojia.net. The default transit departure time is set at 8AM on Monday.
+ **Resource URL**
	+ ``` https://api.zhuwojia.net/api/homestays/commute ```
+ **Resource Information**

	+	|  |  |
		| ------ | ----------- |
		| Response formats   | JSON |
		| Requires authentication?	 | Yes|
		| Rate limited    | No |

+ **Parameters**
	+	| Name | Required  | Description | Default Value | Example
		| ------ | ----------- | ---------- | ---------- | ------ |
		| school_id   | yes | ID of the school | | school
		| homestay_id   | yes | ID of homestay | |5926
		| mode	 | optional| One of these values : *transit*, *driving*, *walking*, *bycycling* | | 22 

+ **Example Request**
	+ ``` https://api.zhuwojia.net/api/homestays/commute?school_id=14873&homestay_id=HS100001&mode=transit ```

+ **Example Response**

	+ ``` 
    [{
	    "time": "45 minutes ",
	    "transfer": "0",
	    "time_in_second": "2749"
	},]  ```

## Get Homestay by id

* ```GET api/homestay/(homestay id )```

+ Return a collection of cities or schools.
+ **Resource URL**
	+ ``` https://api.zhuwojia.net/api/homestay/(homestay id ) ```
+ **Resource Information**

	+	|  |  |
		| ------ | ----------- |
		| Response formats   | JSON |
		| Requires authentication?	 | Yes|
		| Rate limited    | No |

+ **Parameters**
	+	| Name | Required  | Description | Default Value | Example
		| ------ | ----------- | ---------- | ---------- | ------ |
		| host_id   | yes | The homestay id value  | | HS100001

+ **Response Field Explained**
	+	| Name | Description | Example
		| ------ | ----------- | ------ |
		| fee | lowest value of above the ground rooms | 800
		| charge | lowest value of basement rooms | 600
		| price | value of each individual room | 800

+ **Example Request**
	+ ``` https://api.zhuwojia.net/api/homestay/HS100001 ```

+ **Example Response**

	+ ``` 
    [{
    "resitype": "thouse",
    "addline1": "27 Kennaley Cres.",
    "citytown": "Scarborough",
    "provincestate": "Ontario",
    "zippost": "M1V1L6",
    "Country": "Canada",
    "prptype": "c",
    "alptype": "",
    "tmember": "2",
    "agroom": 2,
    "bsroom": 0,
    "sharewash": "2",
    "breakfast": "something hahaha",
    "bchef": "n",
    "lunch": "this is a get",
    "lchef": "n",
    "dinner": "safdsadf",
    "dchef": "n",
    "foodlimit": "",
    "net": "y",
    "tv": "y",
    "communication": "",
    "language": "en",
    "otherlanguage": "",
    "activity": "",
    "music": "",
    "cat": 0,
    "dog": 1,
    "opet": "",
    "source": "",
    "fee": 400,
    "charge": 400,
    "smoke": "n",
    "gpr": "np",
    "minage": "np",
    "maxage": "np",
    "fn2": "first",
    "ln2": "Last",
    "gr1": "M",
    "gr2": "F",
    "dy1": "1972",
    "dy2": "1975",
    "cb1": "North America",
    "cb2": "North America",
    "in1": "Education services",
    "in2": "Real estate and rental and leasing",
    "oc1": "Staff",
    "oc2": "Staff",
    "washer_dryer": "n",
    "lat": 43.81371494585236,
    "lng": -79.28394695914749,
    "id": "HS100921",
    "agavail": "2020-12-24",
    "bravail": "2020-12-24",
    "visited": "n",
    "police_check": "n",
    "opetzh": "",
    "regulation": "this is a test.\nadsf\ntest line break",
    "selfintro": "Carpenter",
    "rp1": "Male host",
    "rp2": "spouse",
    "host_id": 2508,
    "last_modified": "2020-07-20 11:40:47",
    "promo": "y",
    "id_db": 921,
    "host_name": "Emily",
    "status": 1,
    "user_created": "2020-07-09 09:42:52",
    "meal_price": "1.00",
    "interviews": [
        {
            "id": 2236,
            "vtype": "staff",
            "name": "",
            "hid": "HS100921",
            "date": "2019-12-30",
            "quality": 5,
            "comment": "This is a test comment.\n\nIt's a good homestay. I highly recommend.",
            "feedback": null,
            "sid": null,
            "liked": 0,
            "host_reply": null,
            "host_reply_date": null,
            "review_type": 0
        }
    ],
    "badges": {
        "interview": false,
        "interview_student": false,
        "police_check": false,
        "institution": false,
        "area": [],
        "background_check_year": 0
    },
    "rooms": [
        {
            "id": 1,
            "hid": "HS100921",
            "price": "600",
            "available_date": "2020-07-16",
            "size": 100,
            "type": "ground",
            "washroom": "shared"
        },
        {
            "id": 2,
            "hid": "HS100921",
            "price": "500",
            "available_date": "2020-07-27",
            "size": 100,
            "type": "basement",
            "washroom": "shared"
        }
    ]
}] ```


## Get random interviewed homestay

* ```GET api/homestays/random```

+ Return a random interviewed homestay.
+ **Resource URL**
	+ ``` https://api.zhuwojia.net/api/homestays/random ```
+ **Resource Information**

	+	|  |  |
		| ------ | ----------- |
		| Response formats   | JSON |
		| Requires authentication?	 | Yes|
		| Rate limited    | No |

+ **Parameters**
	+	| Name | Required  | Description | Default Value | Example
		| ------ | ----------- | ---------- | ---------- | ------ |
		| not_in   | optional | List homestay id that be filtered out , separe by ***,*** | | HS100001,HS100002,HS1000003
		

+ **Example Request**
	+ ``` https://api.zhuwojia.net/api/homestays/random ```
+ **Example Response**
	
	+ ``` 
    [{
	    "resitype": "condo",
	    "addline1": "113 Sienna Park Green SW",
	    "citytown": "Calgary",
	    "provincestate": "Alberta",
	    "zippost": "M2J5B3",
	    "Country": "Canada",
	    "prptype": "c",
	    "alptype": "",
	    "tmember": "2",
	    "agroom": 10,
	    "bsroom": 10,
	    "sharewash": "10",
	    "breakfast": "Some breakfastadsfasf",
	    "bchef": "b",
	    "lunch": "Some lunch",
	    "lchef": "b",
	    "dinner": "Some dinner",
	    "dchef": "b",
	    "foodlimit": "",
	    "net": "y",
	    "tv": "y",
	    "communication": "",
	    "language": "en zh",
	    "otherlanguage": "",
	    "activity": "",
	    "music": "",
	    "cat": 0,
	    "dog": 0,
	    "opet": "",
	    "source": "",
	    "fee": 300,
	    "smoke": "y",
	    "gpr": "np",
	    "minage": "np",
	    "maxage": "np",
	    "addcom": "",
	    "gr1": "F",
	    "gr2": "F",
	    "dy1": "1980",
	    "dy2": "1984",
	    "cb1": "Asia",
	    "cb2": "Europe",
	    "in1": "Management of companies and enterprises",
	    "in2": "Management of companies and enterprises",
	    "oc1": "Product Manager",
	    "oc2": "Product Manager",
	    "washer_dryer": "y",
	    "lat": 43.77819494585236,
	    "lng": -79.33375967815735,
	    "id": "HS100001",
	    "agavail": "2019-12-17",
	    "bravail": "2019-12-17",
	    "visited": "n",
	    "police_check": "y",
	    "opetzh": "",
	    "regulation": "regulation<b>big</b>",
	    "selfintro": "hello<span>aasdfsa</span>",
	    "rp1": "Female host",
	    "rp2": "wife Host",
	    "host_id": 3,
	    "last_modified": "2020-01-15 14:23:40",
	    "promo": "n",
	    "id_db": 1,
	    "host_name": "hnamee"
	    "status": 1,
	    "cityid": 126,
	    "meal_price": "300.00",
	    "city": {
	        "city": "Calgary",
	        "province": "Alberta",
	        "lat": 51.0486151,
	        "lng": -114.0708459,
	        "city_zh": "卡尔加里",
	        "radius": 15,
	        "id": 126,
	        "suggestion_name": "Calgary"
    	}
    }] ```

## Get featured homestay

* ```GET api/homestays/featured```

+ Return featured homestays in Toronto, Vancouver and other cities.
+ **Resource URL**
	+ ``` https://api.zhuwojia.net/api/homestays/featured ```
+ **Resource Information**

	+	|  |  |
		| ------ | ----------- |
		| Response formats   | JSON |
		| Requires authentication?	 | Yes|
		| Rate limited    | No |

+ **Parameters**
	+	| Name | Required  | Description | Default Value | Example
		| ------ | ----------- | ---------- | ---------- | ------ |
		| city   | optional | List featured homestays in Toronto, Vancouver or Other | other | toronto,vancouver,other
		

+ **Example Request**
	+ ``` https://api.zhuwojia.net/api/homestays/featured?city=toronto ```
+ **Example Response**
	
	+ ``` 
    [{
        "id": "HS101085",
        "host_name": "corina",
        "citytown": "Mississauga",
        "fee": 950,
        "provincestate": "Ontario"
    },
    {
        "id": "HS101216",
        "host_name": "Mark",
        "citytown": "Markham",
        "fee": 1450,
        "provincestate": "Ontario"
    }] ```

## Get homestay pictures
* ```GET ajax/photo/photos/(h_id)```
+ Return a collection of homestay pictures IDs and routes. The picture could be accessed by adding domain in front of the id. We use basic http authentication for this api.
+ **Resource URL**
	+ ``` https://www.homadorma.com/ajax/photo/photos/(h_id) ```
+ **Resource Information**

	+	|  |  |
		| ------ | ----------- |
		| Response formats   | JSON |
		| Requires authentication?	 | Yes|
		| Rate limited    | No |

+ **Parameters**
	+	| Name | Required  | Description | Default Value | Example
		| ------ | ----------- | ---------- | ---------- | ------ |
		| type   | optional | set type of host images do you want to get with three option: all,self,staff | staff | all

+ **Example Request**
	+ ``` https://www.homadorma.com/ajax/photo/photos/HS100001?type=all ```
+ **Example Response**
	+ ``` 
    ["success":"true",
    "data":["assets\/imgs\/homestayimages\/HS100001\/18.jpg"]
	}]```

## Get nearby homestay close to a given homestay ID
* ```GET api/nearby```
+ Return a collection of nearby homestay with distance. We use basic http authentication for this api.
+ Homestay IDs returned are within 0.045 plus or minus the input geocode.
+ **Resource URL**
	+ ``` https://api.zhuwojia.net/api/nearby ```
+ **Resource Information**

	+	|  |  |
		| ------ | ----------- |
		| Response formats   | JSON |
		| Requires authentication?	 | Yes|
		| Rate limited    | No |

+ **Parameters**
	+	| Name | Required  | Description | Default Value | Example
		| ------ | ----------- | ---------- | ---------- | ------ |
		| hid   | yes | The homestay id value	 |  | HS100001
		| distance   | yes | the distance within (in km)		 |  | 1

+ **Example Request**
	+ ``` https://api.zhuwojia.net/api/nearby?hid=HS100001&distance=0.07 ```
+ **Example Response**
	+ ``` 
    [
    {
        "id": "HS106761",
        "host_name": "Gokche",
        "fee": 850,
        "agroom": 0,
        "bsroom": 2,
        "sharewash": "2",
        "distance": 0.38
    },
    {
        "id": "HS104039",
        "host_name": "Kathrina",
        "fee": 670,
        "agroom": 0,
        "bsroom": 1,
        "sharewash": "2",
        "distance": 0.39
    },
    {
        "id": "HS101658",
        "host_name": "William",
        "fee": 1250,
        "agroom": 4,
        "bsroom": 2,
        "sharewash": "2",
        "distance": 0.4
    }
	]```

## Get nearby school close to a given homestay ID
* ```GET api/nearby-school```
+ Return a collection of nearby schools with school info and ranking. We use basic http authentication for this api. Note the algorithm takes homestays within lattitude and longitude -+0.045. So some homestays in the results returned are not exactly within the distance circle.
+ School IDs returned are within 0.045 plus or minus the input geocode.
+ **Resource URL**
	+ ``` https://api.zhuwojia.net/api/nearby-school ```
+ **Resource Information**

	+	|  |  |
		| ------ | ----------- |
		| Response formats   | JSON |
		| Requires authentication?	 | Yes|
		| Rate limited    | No |

+ **Parameters**
	+	| Name | Required  | Description | Default Value | Example
		| ------ | ----------- | ---------- | ---------- | ------ |
		| hid   | yes | The homestay id value	 |  | HS100001
		| distance   | yes | the distance within (in km)		 |  | 1
		| type   | yes | the type of the school by level of study. <ul>One of these values : <li>"elementary", stands for elementary schools</li><li>"secondary", stands for secondary schools</li><li>"college", stands for colleges and universities</li><li>"language", stands for language schools</li></ul>	 |  | elementary

+ **Example Request**
	+ ``` https://api.zhuwojia.net/api/nearby-school?hid=HS100021&type=elementary&distance=1 ```
+ **Example Response**
	+ ``` 
    [
    {
        "id": 6035,
        "name": "Willowdale Christian School",
        "name_zh": "",
        "board": null,
        "board_zh": null,
        "level": "Elementary",
        "type": "School",
        "address": "60 Hilda Ave",
        "city": "North York",
        "province": "Ontario",
        "zip": "M2M1V5",
        "lat": 43.7866031,
        "lng": -79.4247694,
        "uid": "887307",
        "suggestion_name": "WillowdaleChristianSchool",
        "ranking": null,
        "rating": null,
        "dis": 1.21,
        "total_ranking": 3037
    },
    {
        "id": 3479,
        "name": "Mesorah Montessori School",
        "name_zh": "",
        "board": null,
        "board_zh": "",
        "level": "Elementary",
        "type": "School",
        "address": "613 Clark Avenue",
        "city": "Thornhill",
        "province": "Ontario",
        "zip": "L4J5V3",
        "lat": 43.80365,
        "lng": -79.44406,
        "uid": "665406",
        "suggestion_name": "MesorahMontessoriSchool",
        "ranking": null,
        "rating": null,
        "dis": 1.24,
        "total_ranking": 3037
    },
    {
        "id": 2167,
        "name": "ÉIC Monseigneur-de-Charbonnel",
        "name_zh": "",
        "board": "CS catholique MonAvenir",
        "board_zh": null,
        "level": "7-8",
        "type": "School",
        "address": "110 Drewry avenue",
        "city": "Toronto",
        "province": "Ontario",
        "zip": "M2M1C8",
        "lat": 43.78662,
        "lng": -79.42357,
        "uid": "705128",
        "suggestion_name": "ÉICMonseigneurdeCharbonnel",
        "ranking": null,
        "rating": null,
        "dis": 1.27,
        "total_ranking": 3037
    },
    {
        "id": 2972,
        "name": "JNY Day School",
        "name_zh": "",
        "board": null,
        "board_zh": "",
        "level": "Elementary",
        "type": "School",
        "address": "465 Patricia Avenue",
        "city": "Toronto",
        "province": "Ontario",
        "zip": "M2R2N1",
        "lat": 43.78409,
        "lng": -79.44397,
        "uid": "668272",
        "suggestion_name": "JNYDaySchool",
        "ranking": null,
        "rating": null,
        "dis": 1.46,
        "total_ranking": 3037
    },
    {
        "id": 5029,
        "name": "Birchas Shmuel",
        "name_zh": "",
        "board": null,
        "board_zh": null,
        "level": "Elem/Sec",
        "type": "School",
        "address": "465 Patricia Avenue",
        "city": "North York",
        "province": "Ontario",
        "zip": "M2R2N1",
        "lat": 43.7840889,
        "lng": -79.4449079,
        "uid": "885865",
        "suggestion_name": "BirchasShmuel",
        "ranking": null,
        "rating": null,
        "dis": 1.5,
        "total_ranking": 3037
    }
	]```

## Get new student registered
* ```GET api/student/new```
+ Return a list of new students registered and their respective data in the last number of hours.
+ **Resource URL**
    + ``` https://api.zhuwojia.net/api/student/new ```
+ **Resource Information**

    +   |  |  |
        | ------ | ----------- |
        | Response formats   | JSON |
        | Requires authentication?   | Yes|
        | Rate limited    | No |

+ **Parameters**
    +   | Name | Required  | Description | Default Value | Example
        | ------ | ----------- | ---------- | ---------- | ------ |
        | hour   | yes | The last number of hours to pull    |  | 12

+ **Example Request**
    + ``` https://api.zhuwojia.net/api/student/new?hour=12 ```
+ **Example Response**
    + ``` 
    [
    {
        "id": 20966,
        "move_in_date": "2020-09-09",
        "email": "1234@myapi.com",
        "first_name": "homadorma",
        "nationality": "CN",
        "school_name": "Memorial University of Newfoundland",
        "school_name_zh": "纽芬兰纪念大学",
        "school_address": "230 Elizabeth Ave",
        "school_city": "St. John's",
        "school_postcode": "A1B3X9",
        "school_province": "Newfoundland and Labrador"
    },
    {
        "id": 20967,
        "move_in_date": "2020-09-05",
        "email": "1234@myapi.com",
        "first_name": "homadorma",
        "nationality": "CA",
        "school_name": "Alpha Secondary School",
        "school_name_zh": "",
        "school_address": "4600 Parker St",
        "school_city": "Burnaby",
        "school_postcode": "V5C3E2",
        "school_province": "British Columbia"
    }
    ]```
## Update post ad value

* ```POST api/update_ad```
+ Update the post ad value for **post_ad_record** table.
+ **Resource URL**
	+ ``` http://api.homadorma.com/api/update_ad ```
+ **Resource Information**

	+	|  |  |
		| ------ | ----------- |
		| Response formats   | JSON |
		| Requires authentication?	 | Yes|
		| Rate limited    | No |

+ **Parameters**
	+	| Name | Required  | Description | Default Value | Example
		| ------ | ----------- | ---------- | ---------- | ------ |
		| type   | yes | One of these values : *facebook*, *mailchimp*,*kijiji*,*craigslist* | | facebook
		| sid   | yes | User id | |5926
		| value	 | yes| One of these values : *0*,*1* | | 1 
	

+ **Example Request**
	+ ``` http://api.homadorma.com/api/update_ad ```

+ **Example Response**
	+ ``` ['status':'OK'] ```	

    